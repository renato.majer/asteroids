import dotenv from 'dotenv';
dotenv.config();
import express from 'express';
import path from 'path';

const app = express();
app.use(express.static(path.join(__dirname, 'public')));

const externalUrl = process.env.RENDER_EXTERNAL_URL;
const port = externalUrl && process.env.PORT ? parseInt(process.env.PORT) : 4080;

app.get('/',function(req, res) { 
    res.sendFile(path.join(__dirname + '/public/index.html'))
});

// start server
if (externalUrl) {
    const hostname = '0.0.0.0';
    app.listen(port, hostname, () => {
    console.log(`Server locally running at http://${hostname}:${port}/ and from outside on ${externalUrl}`);
    });

} else {

    app.listen(port, function () {
        console.log(`Server running at http://localhost:${port}/`);
    });
}