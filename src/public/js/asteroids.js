window.onload = startGame; // start the game when the page loads

/**
 * Represents the type of the game object
 */
const characterType = {
    Player: "Player",
    Asteroid: "Asteroid"
};

const sideOfCanvas = {
    Top: 0,
    Right: 1,
    Bottom: 2,
    Left: 3
};

let window_width = window.innerWidth;
let window_height = window.innerHeight;

// The maximum width and height of asteroid
const asteroidMaxSize = 50;

var player;
var asteroids = [];

var startTime;

const highScoreKey = 'Asteroids_high_score';

/**
 * Main function that starts the game. Called on window load.
 */
function startGame() {

    let player_width = 30;
    let player_height = 30;
    
    // calculate coordinates to create player in the middle of the screen
    let player_x_pos = (window_width / 2);
    let player_y_pos = (window_height / 2);

    // Create player object
    player = new component(player_width, player_height, "red", player_x_pos, player_y_pos, characterType.Player);

    // Start game
    gameArea.start();
}

/**
 * Object for drawing the game scene and controlling the game
 */
var gameArea = {
    canvas : document.getElementById("gameCanvas"),

    start : function() {
        
        this.context = this.canvas.getContext("2d");

        this.canvas.width = window_width;
        this.canvas.height = window_height;

        // Update the game scene every 20ms
        this.updateIntervalID = setInterval(updateGameArea, 20);

        // Generate new asteroids every 300ms
        this.generateAsteroidsIntervalID = setInterval(generateAsteroids, 300);

        startTime = performance.now();
        this.context.font = "30px Verdana";
        this.context.textAlign = "right";
        
        this.context.fillText(`Vrijeme: ${formatTime(0)}`, window_width - 50, 100);
    },

    stop : function() {
        // Stop updating the game scene and stop generating asteroids
        clearInterval(this.generateAsteroidsIntervalID);
        clearInterval(this.updateIntervalID);
    },

    clear : function() {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }
}

function component(width, height, color, x, y, type) {

    /**
     * Type of game component
     */
    this.type = type;

    this.width = width;
    this.height = height;

    // Setup speed vector
    if(this.type === characterType.Player) {
        this.speed_x = 0;
        this.speed_y = 0;

    } else { // Asteroid
        
        let side = mapCoordinates(x, y); // the side of the canvas the asteroid is generated on
        let speed_x;
        let speed_y;

        // generate the speed vector according to the spawning side of the asteroid
        if(side === sideOfCanvas.Top) {
            // speed_y must be lower than 0
            speed_y = -Math.floor(Math.random() * 5) - 1; // [-5, -1]
            speed_x = Math.floor(Math.random() * 11) - 5; // [-5, 5]

        } else if(side == sideOfCanvas.Right) {
            // speed_x must be lower than 0
            speed_y = Math.floor(Math.random() * 11) - 5; // [-5, 5]
            speed_x = -Math.floor(Math.random() * 5) - 1; // [-5, -1]

        } else if(side == sideOfCanvas.Bottom) {
            // speed_y should be greater than 0
            speed_y = Math.floor(Math.random() * 5) + 1; // [1, 5]
            speed_x = Math.floor(Math.random() * 11) - 5; // [-5, 5]

        } else { // Left
            // speed_x should be greater than 0
            speed_y = Math.floor(Math.random() * 11) - 5; // [-5, 5]
            speed_x = Math.floor(Math.random() * 5) + 1; // [1, 5]
        }

        this.speed_x = speed_x;
        this.speed_y = speed_y;
    }

    // Set the object location on the canvas
    this.x = x;
    this.y = y;

    this.update = function() {

        context = gameArea.context;
        context.save();

        context.translate(this.x, this.y); // move the canvas this.x horizontally and this.y vertically

        // Create shadow
        context.shadowBlur = 5;
        context.shadowColor = "black";

        if(this.type === characterType.Asteroid) {
            context.strokeStyle = "black"
            context.lineWidth = 2;
            context.strokeRect(this.width / -2, this.height / -2, this.width, this.height);
        }

        context.fillStyle = color;
        context.fillRect(this.width / -2, this.height / -2, this.width, this.height); // draw a rectangle in a way that it is centered

        context.restore();
    }

    this.newPos = function() {

        // For the Asteroid type only update the coordinates according to previously generated speed vector
        if(this.type === characterType.Asteroid) {
            this.x += this.speed_x;
            this.y += this.speed_y;
            return;
        }
        
        // Update speed vectors according to the keys pressed
        if(keys.ArrowRight) {
            this.speed_x = 2;
        } else if(keys.ArrowLeft) {
            this.speed_x = -2;
        } else {
            this.speed_x = 0;
        }

        if(keys.ArrowUp) {
            this.speed_y = 2;
        } else if(keys.ArrowDown) {
            this.speed_y = -2;
        } else {
            this.speed_y = 0;
        }

       
        if ((this.x - this.width / 2 < 0) && keys.ArrowLeft) // the component has reached the start of the canvas
            this.speed_x = 0;

        else if (((this.x + this.width / 2) >= gameArea.context.canvas.width) && keys.ArrowRight) // the component has reached the end of the canvas
            this.speed_x = 0;

        if ((this.y - this.height / 2 < 0) && keys.ArrowUp) // the component has reached the top of the canvas
            this.speed_y = 0;

        else if (((this.y + this.height / 2) >= gameArea.context.canvas.height) && keys.ArrowDown) // the component has reached the bottom of the canvas
            this.speed_y = 0;


        // update the component coordinates according to the calculated speed vector
        this.x += this.speed_x;
        this.y -= this.speed_y;
    }
} 

/**
 * Generate asteroid with random size and place it on randomly generated side of canvas.
 * When the sateroid is generated, it is not visible to the player.
 */
function generateAsteroids() {
    let side = Math.floor(Math.random() * 4); // The side of the canvas the asteroid will be generate on
    let size = Math.floor(Math.random() * 21) + 30; // [30, 50]

    let y_coord;
    let x_coord;

    // Calculate the coordinates so that the generated asteroid is not visible initially
    if(side === sideOfCanvas.Top) {
        y_coord = -asteroidMaxSize;
        x_coord = Math.floor(Math.random() * window_width);

    } else if (side === sideOfCanvas.Right) {
        y_coord = Math.floor(Math.random() * window_height);
        x_coord = window_width + asteroidMaxSize;

    } else if (side === sideOfCanvas.Bottom) {
        y_coord = window_height + asteroidMaxSize;
        x_coord = Math.floor(Math.random() * window_width);

    } else { // Left side of the canvas
        y_coord = Math.floor(Math.random() * window_height);
        x_coord = - asteroidMaxSize;
    }

    // Choose random color (shade of gray)
    const colors = ["#a9a9a9", "#808080", "#4d4d4d"]
    let colorIndex = Math.floor(Math.random() * 3);
    
    asteroids.push(new component(size, size, colors[colorIndex], x_coord, y_coord, characterType.Asteroid));
}

/**
 * Function for upadating game area. The function is being called every 20ms.
 */
function updateGameArea() {

    gameArea.clear();

    player.newPos(); // calculate new position
    player.update(); // draw the player

    // Remove the asteroids which are not visible any more
    asteroids = asteroids.filter(asteroid => {
        return checkIsVisible(asteroid)
    })

    // Update asteroids positions
    asteroids.forEach((asteroid, index) => {
        asteroid.newPos();
        asteroid.update();
    })

    // Show current time and best time
    let currentTime = performance.now()
    let timeString = formatTime(currentTime - startTime);

    let value;
    if(!localStorage.getItem(highScoreKey)) {
        value = 0;
    } else {
        value = localStorage.getItem(highScoreKey);
    }

    this.context.fillText(`Najbolje vrijeme: ${formatTime(value)}`, window_width - 50, 50);
    this.context.fillText(`Vrijeme: ${timeString}`, window_width - 50, 100);

    // Check if there is collision between player and any of the asteroids
    if(checkCollision()) {
        gameArea.stop();

        // Save high score
        if (typeof (Storage) !== "undefined") { // Check if localStorage is supported
            if(!localStorage.getItem(highScoreKey)) { // No record in localStorage

                localStorage.setItem(highScoreKey, currentTime - startTime);
                
            } else {
                
                let currentHighScore = localStorage.getItem(highScoreKey);

                if(currentTime - startTime > currentHighScore) { // check if the new score is high score
                    localStorage.setItem(highScoreKey, currentTime - startTime);
                }
            }
        }

        alert('Game over!');
    }
}

/**
 * Helper function that returns the side of the canvas for the given coordinates
 * @param {*} x x coordinate
 * @param {*} y y coordinate
 */
function mapCoordinates(x, y) {
    if(x <= 0) {
        return sideOfCanvas.Left
    } else if(x >= window_width) {
        return sideOfCanvas.Right
    } else if (y <= 0) {
        return sideOfCanvas.Top
    } else { // Bottom
        return sideOfCanvas.Bottom
    }
}

/**
 * Helper function that checks if the asteroid is visible to the player.
 */
function checkIsVisible(asteroid) {

    if (asteroid.x < -asteroidMaxSize || asteroid.x > window_width + asteroidMaxSize) {
        // the asteroid is out of the screen on the left or right side
        return false;
    } else if (asteroid.y < -asteroidMaxSize || asteroid.y > window_height + asteroidMaxSize) {
        // the asteroid is out of the screen on top or bottom side
        return false;
    }

    return true; // asteroid is still visible or it is going to be visible
}

/**
 * Checks if there is any collision between the player and any of the asteroids.
 * Returns true if the collision is detected, otherwise returns false.
 */
function checkCollision() {
    let player_start_x = player.x - (player.width / 2);
    let player_end_x = player.x + (player.width / 2);
    let player_start_y = player.y - (player.height / 2);
    let player_end_y = player.y + (player.height / 2);

    for(const asteroid of asteroids) {
        let ast_start_x = asteroid.x - (asteroid.width / 2);
        let ast_end_x = asteroid.x + (asteroid.width / 2);
        let ast_start_y = asteroid.y - (asteroid.height / 2);
        let ast_end_y = asteroid.y + (asteroid.height / 2);

        if (
            player_end_x >= ast_start_x &&
            player_start_x <= ast_end_x &&
            player_end_y >= ast_start_y &&
            player_start_y <= ast_end_y
        ) {
            return true; // Collision detected
        }
    }

    return false; // No collision detected
}

// Detect when when the arrow key are pressed or released
const keys = {
    ArrowUp: false,
    ArrowLeft: false,
    ArrowRight: false,
    ArrowDown: false
};

function keyEvent(event) {
    if(keys[event.code] !== undefined) { // check if the key should be tracked
        keys[event.code] = event.type === "keydown";
        event.preventDefault();
    }
}

document.addEventListener('keydown', keyEvent);
document.addEventListener('keyup', keyEvent);

/**
 * Helper function for formatting time
 */
function formatTime(milliseconds) {
    // Convert milliseconds into minutes, seconds and milliseconds
    let minutes = Math.floor(milliseconds / (60 * 1000));
    let seconds = Math.floor((milliseconds % (60 * 1000)) / 1000);
    let millis = milliseconds % 1000;

    // Add leading zeros
    minutes = String(minutes).padStart(2, '0');
    seconds = String(seconds).padStart(2, '0');
    millis = String(millis).padStart(3, '0');

    return `${minutes}:${seconds}.${millis}`;
}